@extends('dashboards.admins.layouts.admin-dash-layout')
@section('title','Dashboard')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Posts Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success')) <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
            <a class="btn btn-primary mb-3" href="{{ route('admin.createuser')}}">Create New User</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($data as $key => $users)
                    <tr>
                        <td> {{ $key+1 }} </td>
                        <td> {{ $users->name}} </td>
                        <td> {{ $users->email}} </td>
                        @if($users->role==1)
                        <td> Admin </td>
                        @else
                        <td> Member </td>
                        @endif
                        <td style="display: flex;">
                            <a href="{{ route('admin.showuser', $users->id) }}" class="btn btn-info btn-sm mr-2">Show</a>
                            <a href="{{ route('admin.edituser', $users->id) }}" class="btn btn-default btn-sm mr-2">Edit</a>
                            <a onclick="return confirm('Are you sure?')" href="{{ route('admin.deleteuser', $users->id) }}" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Posts</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
</div>

@endsection